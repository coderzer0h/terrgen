package operators

type Multiply struct {
	Limit  float64
	Offset float64
}

func (m *Multiply) Combine(l, r float64) float64 {
	if l > m.Offset && l < m.Offset+m.Limit {
		return l * r
	} else {
		return l
	}
}

package operators

type Add struct {
	Limit  float64
	Offset float64
	Weight float64
}

func (a *Add) Combine(l, r float64) float64 {
	if l >= a.Offset && l <= a.Offset+a.Limit {
		return l*(1.0-a.Weight) + r*a.Weight
	} else {
		return l
	}
}

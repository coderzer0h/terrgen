package operators

type Operator interface {
	Combine(l, r float64) float64
}

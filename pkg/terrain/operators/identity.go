package operators

type Identity struct {
	Limit  float64
	Offset float64
}

func (i *Identity) Combine(l, r float64) float64 {
	if l >= i.Offset && l <= i.Offset+i.Limit {
		return r
	} else {
		return l
	}
}

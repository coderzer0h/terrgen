package modifiers

import (
	"bitbucket.com/coderzer0h/terrgen/pkg/terrain/operators"
)

// Copy represents a copy of the terrain
type Copy struct {
	Scale float64
}

// Apply applies the simplex noise modifier
func (c *Copy) Apply(x, y uint, height float64, o operators.Operator) float64 {
	h := height * c.Scale
	return o.Combine(height, h)
}

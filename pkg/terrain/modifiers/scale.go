package modifiers

import (
	"bitbucket.com/coderzer0h/terrgen/pkg/terrain/operators"
)

// Scale represents a scaled copy of the terrain
type Scale struct {
	scale float64
}

// Apply applies the simplex noise modifier
func (s *Scale) Apply(x, y uint, height float64, o operators.Operator) float64 {
	h := height * s.scale
	return o.Combine(height, h)
}

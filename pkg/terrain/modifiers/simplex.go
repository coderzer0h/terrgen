package modifiers

import (
	"bitbucket.com/coderzer0h/terrgen/pkg/terrain/operators"
	"github.com/ojrac/opensimplex-go"
)

// Simplex represents a simplex noise modifier
type Simplex struct {
	Seed    int64
	ng      opensimplex.Noise
	XOffset float64
	YOffset float64
	XScale  float64
	YScale  float64
}

// NewSimplex returns a new simplex noise generator
func NewSimplex(seed int64) *Simplex {
	ng := opensimplex.NewNormalized(seed)

	return &Simplex{ng: ng}
}

// SetScale sets the simplex noise scale
func (s *Simplex) SetScale(x, y float64) *Simplex {
	s.XScale = x
	s.YScale = y
	return s
}

// SetOffset sets the offset for simplex noise sampling
func (s *Simplex) SetOffset(x, y float64) *Simplex {
	s.XOffset = x
	s.YOffset = y
	return s
}

// Apply applies the simplex noise modifier
func (s *Simplex) Apply(x, y uint, height float64, o operators.Operator) float64 {
	h := s.ng.Eval2(s.XOffset+(float64(x)*s.XScale), s.YOffset+(float64(y)*s.YScale))
	return o.Combine(height, h)
}

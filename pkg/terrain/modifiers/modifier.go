package modifiers

import "bitbucket.com/coderzer0h/terrgen/pkg/terrain/operators"

// Modifier represents a modifier that modifies the heightmap
type Modifier interface {
	Apply(x, y uint, height float64, m operators.Operator) float64
}

package terrain

import (
	"bitbucket.com/coderzer0h/terrgen/pkg/terrain/modifiers"
	"bitbucket.com/coderzer0h/terrgen/pkg/terrain/operators"
)

type NoiseSample struct {
	XScale  float64
	YScale  float64
	XOffset float64
	YOffset float64
	Gain    float64
	Weight  float64
}

// Point represents a point in a terrain
type Point struct {
	Height float64
}

// Terrain represents a square terrain
type Terrain struct {
	Size   uint
	Points [][]Point
	min    float64
	max    float64
}

// New returns a new empty terrain
func New(size uint) *Terrain {

	var min, max float64
	p := make([][]Point, size)
	var i, j uint
	for i = 0; i < size; i++ {
		p[i] = make([]Point, size)
		for j = 0; j < size; j++ {
			p[i][j] = Point{}

			if p[i][j].Height < min {
				min = p[i][j].Height
			}
			if p[i][j].Height > max {
				max = p[i][j].Height
			}
		}
	}
	return &Terrain{Size: size, Points: p, min: min, max: max}
}

// Apply modifier with operator
func (t *Terrain) Apply(m modifiers.Modifier, o operators.Operator) {
	var x, y uint
	for x = 0; x < t.Size; x++ {
		for y = 0; y < t.Size; y++ {
			t.Points[x][y].Height = m.Apply(x, y, t.Points[x][y].Height, o)
		}
	}
}

// GetUint8ScaledHeight returns the height scaled to a uint8 for greyscale creation
func (t *Terrain) GetUint8ScaledHeight(x, y uint) uint8 {
	if x > t.Size || y > t.Size {
		return 0
	}

	height := t.Points[x][y].Height

	return uint8(256.0 * height)
}

package heightmap

import (
	"image"
	"image/color"
	"image/png"
	"os"

	"bitbucket.com/coderzer0h/terrgen/pkg/terrain"
)

// CreateHeightmap creates a heightmap
func CreateHeightmap(t *terrain.Terrain) {
	topLeft := image.Point{0, 0}
	bottomRight := image.Point{int(t.Size), int(t.Size)}
	img := image.NewGray(image.Rectangle{topLeft, bottomRight})

	var x, y uint
	for x = 0; x < t.Size; x++ {
		for y = 0; y < t.Size; y++ {
			img.Set(int(x), int(y), color.Gray{t.GetUint8ScaledHeight(x, y)})
		}
	}

	f, _ := os.Create("heightmap.png")
	png.Encode(f, img)
}

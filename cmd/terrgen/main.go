package main

import (
	"math/rand"

	"bitbucket.com/coderzer0h/terrgen/pkg/heightmap"
	"bitbucket.com/coderzer0h/terrgen/pkg/terrain/modifiers"
	"bitbucket.com/coderzer0h/terrgen/pkg/terrain/operators"

	"bitbucket.com/coderzer0h/terrgen/pkg/terrain"
)

func main() {
	t := terrain.New(8196)
	s := modifiers.NewSimplex(rand.Int63()).SetScale(0.01, 0.02).SetOffset(1.0, 5.0)
	t.Apply(s, &operators.Assign{})
	s.SetScale(0.01, 0.01).SetOffset(4.0, 3.0)
	t.Apply(&modifiers.Copy{Scale: 0.1}, &operators.Add{Limit: 0.0, Offset: 0.5, Weight: 0.8})
	t.Apply(&modifiers.Copy{Scale: 0.1}, &operators.Add{Limit: 0.5, Offset: 0.5, Weight: 0.2})
	heightmap.CreateHeightmap(t)
}
